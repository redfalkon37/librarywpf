﻿using librarywpf.Model;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace librarywpf.View
{
    /// <summary>
    /// Логика взаимодействия для BookWindow.xaml
    /// </summary>
    public partial class BookWindow : Window
    {
        public Book Book { get; private set; }
        public BookWindow(Book book)
        {
            InitializeComponent();
            Book = book;
            DataContext = Book;
        }

        void Accept_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid(this))
                DialogResult = true;
        }

        private void TextBox_Error(object sender, ValidationErrorEventArgs e)
        {
            MessageBox.Show(e.Error.ErrorContent.ToString());
        }

        /// <summary>
        /// Проверка на прохождение валидации
        /// </summary>
        /// <param name="obj">Объект валидации</param>
        /// <returns>Результат проверки на валидность данных</returns>
        private bool IsValid(DependencyObject obj)
        {
            return !Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(IsValid);
        }
    }
}
