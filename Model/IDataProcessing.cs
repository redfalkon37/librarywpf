﻿using librarywpf.Helper;

namespace librarywpf.Model
{
    /// <summary>
    /// Интерфейс для реализации работы с данными
    /// </summary>
    interface IDataProcessing
    {
        /// <summary>
        /// Выгрузка данных
        /// </summary>
        void Load();
        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <returns></returns>
        RelayCommand Add();
        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <returns></returns>
        RelayCommand Remove();
        /// <summary>
        /// Редактирование элемента
        /// </summary>
        /// <returns></returns>
        RelayCommand Edit();
    }
}
