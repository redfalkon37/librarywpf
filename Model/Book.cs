﻿using librarywpf.Helper;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace librarywpf.Model
{
    /// <summary>
    /// Модель книги
    /// </summary>
    public class Book : INotifyPropertyChanged, IDataErrorInfo
    {
        private string? author;
        private string? name;
        private BookSubjects? subject;
        private int? publicationYear;

        public int Id { get; set; }
        public string? Author
        {
            get => author;
            set
            {
                author = value;
                OnPropertyChanged("Author");
            }
        }
        public string? Name
        {
            get => name;
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public BookSubjects? Subject
        {
            get => subject;
            set
            {
                subject = value;
                OnPropertyChanged("Subjects");
            }
        }
        public int? PublicationYear
        {
            get => publicationYear;
            set
            {
                publicationYear = value;
                OnPropertyChanged("PublicationYear");
            }
        }

        /// <summary>
        /// Проверка на валидность
        /// </summary>
        /// <param name="columnName">Наименование колонки</param>
        /// <returns>Ошибку</returns>
        public string this[string columnName]
        { 
            get
            {
                string error = String.Empty;

                switch (columnName)
                {
                    case "Author" :
                        if (Author != null && System.Text.RegularExpressions.Regex.IsMatch(Author, @"\d"))
                        {
                            error = "Поле Автор не должно содержать цифры";
                        }
                        break;
                    case "PublicationYear":
                        int result;
                        if (PublicationYear != null && !int.TryParse(PublicationYear.ToString(), out result))
                        {
                            error = string.Format("Поле Год выпуска не должно содержать буквы - {0}", result);
                        }
                        break;
                }

                return error;
            }
        }

        public string Error => throw new NotImplementedException();

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
