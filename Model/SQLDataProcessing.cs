﻿using librarywpf.Helper;
using librarywpf.View;
using Microsoft.EntityFrameworkCore;
using System.Collections.ObjectModel;

namespace librarywpf.Model
{
    /// <summary>
    /// Работа с данными через базу данных
    /// </summary>
    class SQLDataProcessing : IDataProcessing
    {
        ApplicationContext db = new ApplicationContext();

        RelayCommand? addCommand;
        RelayCommand? editCommand;
        RelayCommand? deleteCommand;
        public ObservableCollection<Book> Books { get; set; }
        public RelayCommand Add()
        {
            return addCommand ?? (addCommand = new RelayCommand((o) =>
            {
                BookWindow bookWindow = new BookWindow(new Book());
                if (bookWindow.ShowDialog() == true)
                {
                    Book book = bookWindow.Book;
                    db.Books.Add(book);
                    db.SaveChanges();
                }
            }));
        }

        public RelayCommand Edit()
        {
            return editCommand ?? (editCommand = new RelayCommand((selectedItem) =>
            {
                // Получаем выбранную книгу
                Book? book = selectedItem as Book;
                if (book == null) return;

                Book temp = new Book
                {
                    Id = book.Id,
                    Author = book.Author,
                    Name = book.Name,
                    PublicationYear = book.PublicationYear,
                    Subject = book.Subject
                };
                BookWindow bookWindow = new BookWindow(temp);

                if (bookWindow.ShowDialog() == true)
                {
                    book.Author = bookWindow.Book.Author;
                    book.Name = bookWindow.Book.Name;
                    book.PublicationYear = bookWindow.Book.PublicationYear;
                    book.Subject = bookWindow.Book.Subject;
                    db.Entry(book).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }));
        }

        public void Load()
        {
            db.Database.EnsureCreated();
            db.Books.Load();
            Books = db.Books.Local.ToObservableCollection();
        }

        public RelayCommand Remove()
        {
            return deleteCommand ?? (deleteCommand = new RelayCommand((selectedItem) =>
            {
                //Получаем выбранную книгу
                Book? book = selectedItem as Book;
                if (book == null) return;
                db.Books.Remove(book);
                db.SaveChanges();
            }));
        }
    }
}
