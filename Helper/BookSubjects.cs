﻿using System.ComponentModel;

namespace librarywpf.Helper
{
    /// <summary>
    /// Данный список тематик взята на основе 10 часто встречаемых тематик книг
    /// </summary>
    public enum BookSubjects
    {
        //Осуждение
        [Description("Осуждение")]
        Condemnation,
        //Выживание
        [Description("Выживание")]
        Survival,
        //Война и мир
        [Description("Война и мир")]
        WarAndPiece,
        //Любовь
        [Description("Любовь")]
        Love,
        //Героизм
        [Description("Героизм")]
        Heroism,
        //Добро и зло
        [Description("Добро и зло")]
        GoodAndEvil,
        //Жизненный цикл
        [Description("Жизненный цикл")]
        LifeCycle,
        //Страдание
        [Description("Страдание")]
        Suffering,
        //Взросление
        [Description("Взросление")]
        GrowingUp,
        //Обман
        [Description("Обман")]
        Deception
    }
}
