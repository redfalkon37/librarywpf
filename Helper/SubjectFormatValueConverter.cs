﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Windows.Data;

namespace librarywpf.Helper
{
    /// <summary>
    /// Преобразование поля enum в его значение
    /// </summary>
    public class SubjectFormatValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is BookSubjects subject)
            {
                return GetString(subject);
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string s)
            {
                Type type = typeof(BookSubjects);
                FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Static);
                foreach (FieldInfo field in fields)
                {
                    DescriptionAttribute attribute = field.GetCustomAttribute<DescriptionAttribute>();
                    if (attribute != null && attribute.Description == s)
                    {
                        return (BookSubjects)field.GetValue(null);
                    }
                }

            }
            return null;
        }

        public string[] Strings => GetStrings();

        public static string GetString(BookSubjects subject)
        {
            return GetDescription(subject);
        }

        public static string GetDescription(BookSubjects subject)
        {
            return subject.GetType().GetMember(subject.ToString())[0].GetCustomAttribute<DescriptionAttribute>().Description;

        }
        public static string[] GetStrings()
        {
            List<string> list = new List<string>();
            foreach (BookSubjects subject in Enum.GetValues(typeof(BookSubjects)))
            {
                list.Add(GetString(subject));
            }

            return list.ToArray();
        }
    }
}
