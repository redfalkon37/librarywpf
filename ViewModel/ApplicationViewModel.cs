﻿using librarywpf.Helper;
using librarywpf.Model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Data;

namespace librarywpf.ViewModel
{
    public class ApplicationViewModel
    {
        // Определяем обработчик работы с данными
        SQLDataProcessing dataProcessing = new SQLDataProcessing();

        public ObservableCollection<Book> Books { get; set; }

        CollectionView view;
        RelayCommand? clearFilterCommand;

        private string selectedAuthor;
        private int selectedYear;

        public ApplicationViewModel()
        {
            dataProcessing.Load();
            Books = dataProcessing.Books;

            view = (CollectionView)CollectionViewSource.GetDefaultView(Books);
            view.Filter = UserFilter;
        }

        //Добавление
        public RelayCommand AddCommand => dataProcessing.Add();

        //Редактирование
        public RelayCommand EditCommand => dataProcessing.Edit();

        //Удаление
        public RelayCommand DeleteCommand => dataProcessing.Remove();

        //Очистка фильтров
        public RelayCommand ClearFilterCommand
        {
            get
            {
                return clearFilterCommand ?? (clearFilterCommand = new RelayCommand((o) =>
                {
                    SelectedAuthor = string.Empty;
                    SelectedYear = default;
                }));
            }
        }

        public List<string> AuthorFilter
        {
            get
            {
                if (Books.Any())
                    return Books.Select(x => x.Author).ToList();
                else
                    return new List<string>();
            }
        }

        public List<int?> YearFilter
        {
            get
            {
                if (Books.Any())
                    return Books.Select(x => x.PublicationYear).ToList();
                else
                    return new List<int?>();
            }
        }

        public string SelectedAuthor
        {
            get => selectedAuthor;
            set
            {
                selectedAuthor = value;
                selectedYear = default;
                view.Refresh();
            }
        }

        public int SelectedYear
        {
            get => selectedYear;
            set
            {
                selectedYear = value;
                selectedAuthor = string.Empty;
                view.Refresh();
            }
        }

        //Включение фильтрации
        private bool UserFilter(object item)
        {
            clearFilterCommand = ClearFilterCommand;
            if (string.IsNullOrEmpty(selectedAuthor) && selectedYear == default)
                return true;
            var book = (Book)item;

            return !string.IsNullOrEmpty(selectedAuthor) ? book.Author == selectedAuthor : 
                selectedYear != default ? book.PublicationYear == selectedYear : true;
        }
    }
}
